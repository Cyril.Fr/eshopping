import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: "root",
})
export class CategoryService {
  constructor(private db: AngularFireDatabase) {}

  // Get the list of categories from database
  /* getCategories() {
    return this.db
      .list("/categories", (ref) => ref.orderByChild("name"))
      .valueChanges();
  } */

  // Get the list of categories from database
  getAll() {
    return this.db
      .list("/categories", (ref) => ref.orderByChild("name"))
      .snapshotChanges()
      .pipe(
        map((categories) =>
          categories.map((c) => ({ key: c.payload.key, ...c.payload.val() }))
        )
      );
  }
}
