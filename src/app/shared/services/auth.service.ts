import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from "firebase";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "./user.service";
import { User } from "shared/models/user.model";
import { switchMap } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  public user$: Observable<firebase.User>;

  constructor(
    private userService: UserService,
    private afAuth: AngularFireAuth,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.user$ = afAuth.authState;
  }
  /* 
    Get the current route and extract the returnUrl parameter
    if the returnUrl doesn't exist use the router of website
    next store the returnUrl in localStrorage
    then redirect the user to google for authentication page
    if user is log in makes sure to update name stored in database
    read the returnURL param in local storage 
    and redirect to this returnUrl
  */
  login() {
    let returnUrl =
      this.activatedRoute.snapshot.queryParamMap.get("returnUrl") || "/";
    localStorage.setItem("returnUrl", returnUrl);
    this.afAuth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((userCredential) => {
        if (userCredential) {
          this.userService.save(userCredential.user);
          let returnUrl = localStorage.getItem("returnUrl");
          this.router.navigateByUrl(returnUrl);
          console.log("user connected : ", userCredential);
        }
      });
  }

  logout() {
    this.afAuth.signOut();
  }

  // get appUser authenticated from database
  get appUser$(): Observable<User> {
    return this.user$.pipe(
      switchMap((user) => {
        if (user) {
          return this.userService.get(user.uid);
        }
        return of(null);
      })
    );
  }
}
