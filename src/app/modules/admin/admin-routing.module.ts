import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'shared/route-guards/auth.guard';

import { AdminOrdersComponent } from './components/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './components/admin-products/admin-products.component';
import { ProductCreationComponent } from './components/product-creation/product-creation.component';
import { AdminAuthGuard } from './route-guards/admin-auth.guard';

const routes: Routes = [
  {
    path: "products/new",
    component: ProductCreationComponent,
    canActivate: [AuthGuard, AdminAuthGuard],
  },
  {
    path: "products/:id",
    component: ProductCreationComponent,
    canActivate: [AuthGuard, AdminAuthGuard],
  },
  {
    path: "products",
    component: AdminProductsComponent,
    canActivate: [AuthGuard, AdminAuthGuard],
  },
  {
    path: "orders",
    component: AdminOrdersComponent,
    canActivate: [AuthGuard, AdminAuthGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
