import { Component, Input, OnInit } from '@angular/core';
import { CategoryService } from 'shared/services/category.service';

@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.css']
})
export class ProductFilterComponent implements OnInit {
  public categories$;

  @Input('category') category;

  constructor(categoriesService: CategoryService) {
    this.categories$ = categoriesService.getAll();
  }

  ngOnInit() {
  }

}
