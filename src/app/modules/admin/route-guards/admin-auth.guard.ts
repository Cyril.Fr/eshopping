import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { AuthService } from "shared/services/auth.service";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AdminAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService
  ) {}

  // Method that return true if the user is admin or false if user is not admin
  canActivate(): Observable<boolean> {
    return this.authService.appUser$.pipe(map((appUser) => appUser.isAdmin));
  }
}
