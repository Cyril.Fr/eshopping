import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { Product } from 'shared/models/product.model';
import { ProductService } from 'shared/services/product.service';

@Component({
  selector: "app-admin-products",
  templateUrl: "./admin-products.component.html",
  styleUrls: ["./admin-products.component.css"],
})
export class AdminProductsComponent implements OnInit, OnDestroy {
  public displayedColumns: string[] = ["title", "price", "edit"];
  public subscription: Subscription;
  // public products: Product[];
  public dataSource: MatTableDataSource<Product>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private productService: ProductService) {

  }

  ngOnInit() {
    this.subscription = this.productService.getAll().subscribe(products => {
      // this.products = products;
      this.initializeTable(products);
    });

    this.productService.getAll().subscribe(prod => console.log('getAll : ', prod));

  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private initializeTable(products: any[]) {
    this.dataSource = new MatTableDataSource(products);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  filter(query: string) {
    /* let filteredProducts = query ?
      this.products.filter(p => p.title.toLowerCase().includes(query.toLowerCase())) :
      this.products;
    this.initializeTable(filteredProducts); */
    this.dataSource.filter = query.trim().toLowerCase();
  }
}
