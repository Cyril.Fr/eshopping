import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from 'shared/services/auth.service';

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  /*
    If the user log in return true otherwise redirect him to the login page and
    get the url param that the user try to access when the user is no log in
   */
  canActivate(route, state: RouterStateSnapshot) {
    return this.authService.user$.pipe(
      map((user) => {
        if (user) {
          return true;
        }
        this.router.navigate(["/login"], {
          queryParams: { returnUrl: state.url },
        });
        return false;
      })
    );
  }
}
