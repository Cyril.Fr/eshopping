import { NgModule } from '@angular/core';
import { SharedModule } from 'shared/shared.module';

import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';



@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent,
    LoginComponent
  ],
  exports: [HeaderComponent],
  imports: [
    SharedModule,
  ]
})
export class CoreModule { }
