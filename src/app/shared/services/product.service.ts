import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { FirebaseService } from "./firebase.service";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class ProductService {
  constructor(
    private db: AngularFireDatabase,
    private firebaseService: FirebaseService
  ) { }

  // Create product in database
  create(product) {
    this.db.list("/products").push(product);
  }

  // Get the list of products from database
  getAll() {
    return this.db
      .list("/products")
      .snapshotChanges()
      .pipe(
        map((products) =>
          products.map((p) => ({ key: p.payload.key, ...p.payload.val() as {} }))
        )
      );
  }

  // Get the list of products from database
  /* getAll2() {
    // this.db.database.ref("products").on("value", (snapshot) => snapshot.val());
    return this.firebaseService.getData("products").on("value", a => a.val());
  } */

  getById(productId) {
    return this.db.object('/products/' + productId).valueChanges();
  }

  update(productId, product) {
    return this.db.object('/products/' + productId).update(product);
  }

  delete(productId) {
    return this.db.object('/products/' + productId).remove();
  }
}
