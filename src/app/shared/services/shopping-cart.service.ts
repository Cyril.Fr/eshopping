import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Product } from 'shared/models/product.model';
import { take, map } from "rxjs/operators";
import { Observable } from 'rxjs';
import { ShoppingCart } from 'shared/models/shopping-cart.model';
import { ShoppingCartItem } from 'shared/models/shopping-cart-item.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  constructor(private db: AngularFireDatabase) { }

  // Get a shopping-cart
  async getCart(): Promise<Observable<ShoppingCart>> {
    let cartId = await this.getOrCreateCartId();
    return this.db.object('/shopping-carts/' + cartId).valueChanges()
      .pipe(map((shoppingCart: { items: { [productId: string]: ShoppingCartItem } }) => new ShoppingCart(shoppingCart.items)));
  }

  async addToCart(product: Product) {
    this.updateItem(product, 1);
  }

  async removeFromCart(product: Product) {
    this.updateItem(product, -1);
  }

  async clearCart() {
    let cartId = await this.getOrCreateCartId();
    this.db.object('/shopping-carts/' + cartId + '/items/').remove()
  }

  // Create shopping-cart
  private create() {
    return this.db.list('/shopping-carts').push({ dateCreated: new Date().getTime() });
  }

  private getItem(cartId: string, productId: string) {
    return this.db.object('/shopping-carts/' + cartId + '/items/' + productId);
  }

  private async getOrCreateCartId(): Promise<string> {
    let cartId = localStorage.getItem('cartId');
    // return the shopping-cart Id if cartId already exist
    if (cartId) return cartId;

    // Create shopping cart if doesn't exist and store it in localstorage
    let result = await this.create();

    localStorage.setItem('cartId', result.key);

    // return the shopping-cart Id
    return result.key;
  }

  // method that storing or updating our shopping Cart object
  private async updateItem(product: Product, change: number) {
    let cartId = await this.getOrCreateCartId();
    let item$ = this.getItem(cartId, product.key);
    item$.snapshotChanges().pipe(take(1)).subscribe(item => {
      let quantity = (item.payload.child("/quantity").val() || 0) + change;
      if (quantity === 0) item$.remove();
      else item$.update({
        title: product.title,
        imageUrl: product.imageUrl,
        price: product.price,
        quantity: quantity
      });
    });
  }
}
