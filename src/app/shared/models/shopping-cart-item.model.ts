import { Product } from './product.model';

export class ShoppingCartItem {
    key: string;
    title: string;
    imageUrl: string;
    price: number;
    quantity: number;

    constructor(init?: Partial<ShoppingCartItem>) {
        /* Copy all those properties of these init into this current object
         when we have more than one properties */
        Object.assign(this, init);
    }

    get totalPrice() {
        return this.price * this.quantity;
    }
}
