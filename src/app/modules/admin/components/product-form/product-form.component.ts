import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidators } from 'ng2-validation';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { CategoryService } from 'shared/services/category.service';
import { ProductService } from 'shared/services/product.service';

@Component({
  selector: "app-product-form",
  templateUrl: "./product-form.component.html",
  styleUrls: ["./product-form.component.css"],
})
export class ProductFormComponent implements OnInit, OnDestroy {
  public categories$;
  public productForm: FormGroup;
  public product = {};
  public id;
  public subscription: Subscription;

  @Output() productFormData: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  constructor(
    categoryService: CategoryService,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.categories$ = categoryService.getAll();
    /*
      We can take only one value from her observable 
      then the observable will automatically complete 
      so we don't have explecitly unsubscribe because this observable not going emit new value
    */
    this.id = this.route.snapshot.paramMap.get("id");
    if (this.id) {
      this.subscription = this.productService
        .getById(this.id)
        .pipe(take(1))
        .subscribe((p) => (this.product = p));
    }
  }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      price: ["", [Validators.required, CustomValidators.min(0)]],
      category: ["", [Validators.required]],
      imageUrl: ["", [Validators.required, CustomValidators.url]],
    });

    this.productForm.valueChanges.subscribe(
      (value) => {
        console.log(value);
        this.productFormData.next(value);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onSubmit(newProduct) {
    this.id
      ? this.productService.update(this.id, newProduct)
      : this.productService.create(newProduct);
    this.router.navigate(["/admin/products"]);
  }

  delete() {
    if (!confirm('are you sure you want to delete this product ?')) return;
    this.productService.delete(this.id);
    this.router.navigate(["/admin/products"]);
  }
}
