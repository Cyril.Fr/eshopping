import { ShoppingCartItem } from './shopping-cart-item.model';
import { Product } from './product.model';

export class ShoppingCart {
    items: ShoppingCartItem[] = [];

    constructor(private itemsMap: { [productId: string]: ShoppingCartItem }) {
        // make sure that itemsMap object is in right state
        this.itemsMap = itemsMap || {};
        for (let productId in itemsMap) {
            /*
                item is a shoppingCartItem that we get from Firebase
                and it doesn't have the get totalPrice method 
                that we have in your shoppingCartItem model

                So we need to convert this item from type shoppingCartItem of Firebase
                to your shoppingCartItem model
             */

            let item = itemsMap[productId];

            this.items.push(new ShoppingCartItem({ ...item, key: productId }));
        }
    }

    // Get quantity of selected product
    getQuantity(product: Product) {
        // get a reference to that shopping cart item
        let item = this.itemsMap[product.key];
        return item ? item.quantity : 0;
    }

    get totalPrice() {
        let sum = 0;
        for (let itemIndex in this.items)
            sum += this.items[itemIndex].totalPrice;
        return sum;
    }

    get totalItemsCount() {
        let count = 0;
        for (let productId in this.itemsMap)
            count += this.itemsMap[productId].quantity;
        return count;
    }
}
