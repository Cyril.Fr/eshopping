import { Injectable } from "@angular/core";
import * as firebase from "firebase";
import { environment } from "../../../environments/environment";

firebase.initializeApp(environment.firebase);

@Injectable({
  providedIn: "root",
})
export class FirebaseService {
  constructor() {}

  // CREATE
  create(table, data) {
    this.getData(table).push(data);
  }
  
  // READ
  getData(table) {
    return firebase.database().ref(table);
  }

  // UPDATE

  // DELETE
  remove(segment) {
    return firebase.database().ref(segment).remove();
  }
}
