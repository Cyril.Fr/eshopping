import { Component, OnInit } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Component({
  selector: "app-product-creation",
  templateUrl: "./product-creation.component.html",
  styleUrls: ["./product-creation.component.css"],
})
export class ProductCreationComponent implements OnInit {
  public dataProductForm: BehaviorSubject<FormGroup> = new BehaviorSubject<FormGroup>(null);
  
  constructor() {}

  ngOnInit() {}

  onChangeProductForm(newProductData) {
    this.dataProductForm.next(newProductData);
  }
}
