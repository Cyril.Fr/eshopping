export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCab3pIC1yRTtCwxHheQgKXM8cTXfxDmtY",
    authDomain: "eshopping-ac814.firebaseapp.com",
    databaseURL: "https://eshopping-ac814.firebaseio.com",
    projectId: "eshopping-ac814",
    storageBucket: "eshopping-ac814.appspot.com",
    messagingSenderId: "649664715057",
    appId: "1:649664715057:web:e10680fa661e3fbcdd3810",
    measurementId: "G-EPXKNL7QXV"
  }
};
