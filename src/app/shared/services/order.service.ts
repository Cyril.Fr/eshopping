import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ShoppingCartService } from './shopping-cart.service';
import { map } from 'rxjs/operators';
import { Order } from 'shared/models/order.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private db: AngularFireDatabase, private shoppingCartService: ShoppingCartService) { }

  async placeOrder(order: Order) {
    let result = await this.db.list('/orders').push(order);
    this.shoppingCartService.clearCart();
    return result;
  }

  getOrders(): Observable<Order[]> {
    return this.db.list('/orders').valueChanges().pipe(map((order: Order[]) => order));
  }

  getOrdersByUser(userId: string): Observable<Order[]> {
    return this.db.list('/orders', ref =>
      ref.orderByChild('userId').equalTo(userId)).valueChanges().pipe(map((order: Order[]) => order));
  }
}
